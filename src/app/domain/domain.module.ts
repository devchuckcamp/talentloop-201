import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { adminLteConf } from '../admin-lte.conf';


import { DomainRoutingModule } from './domain-routing.module';
import { CoreModule } from '../core/core.module';

import { LayoutModule } from 'angular-admin-lte';

import { DomainComponent } from './domain.component';
import { HomeComponent } from '../home/home.component';

import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';

// Dashboard
import { DashboardModule } from './dashboard/dashboard.module';
import { DashboardRoutingModule } from './dashboard/dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

// Employee
// import { EmployeeModule } from './employee/employee.module';
// import { EmployeeRoutingModule } from './employee/employee-routing.module';
// import { EmployeeComponent } from './employee/employee.component';

// Guard
import { AuthGuard } from '../auth';
import { AuthDomainGuard } from '../component/auth/auth-domain';

// Services
import { GlobalRoutesService } from '../services/route.global';
import { ClientGlobalRoutesService } from '../services/client.global';
import { AuthService } from '../services/auth/auth.service';


@NgModule({
  imports: [
    CommonModule,
    DomainRoutingModule,
    CoreModule,
    LoadingPageModule, 
    MaterialBarModule,
  ],
  declarations: [
    DomainComponent
  ],
  providers:[
    AuthGuard,
    AuthDomainGuard,
    ClientGlobalRoutesService,
    GlobalRoutesService,
    AuthService,
    // UserService
  ],
})
export class DomainModule {}
