import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { adminLteConf } from '../../admin-lte.conf';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { CoreModule } from '../../core/core.module';

import { LayoutModule } from 'angular-admin-lte';

import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from '../../home/home.component';

import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';

// Guard
import { AuthGuard } from '../../auth';
import { AuthDomainGuard } from '../../component/auth/auth-domain';

// Services
import { GlobalRoutesService } from '../../services/route.global';
import { ClientGlobalRoutesService } from '../../services/client.global';
import { AuthService } from '../../services/auth/auth.service';


@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CoreModule,
    LoadingPageModule, MaterialBarModule,
  ],

  declarations: [
    DashboardComponent,
  ],
  providers:[
    AuthGuard,
    AuthDomainGuard,
    ClientGlobalRoutesService,
    GlobalRoutesService,
    AuthService,
    // UserService
  ],
})
export class DashboardModule {}
