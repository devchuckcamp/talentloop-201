import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from '../../auth';
import { AuthDomainGuard } from '../../component/auth/auth-domain';


const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    data: {
        title: 'Get Started',
        customLayout: true
    },
    component: DashboardComponent,
    // children:[
    //   {
    //     path: 'employee',
    //     // component:DashboardComponent,
    //     loadChildren: './employee/employee.module#EmployeeModule',
    //     data: {
    //       title: 'Employee',
    //       customLayout: true
    //     }
    //   }, 
    // ]
  } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
