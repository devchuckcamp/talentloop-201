import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { LayoutService } from 'angular-admin-lte';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user/user.service';

import { Employee } from '../../models/user/employee/index';

import { ReactiveFormsModule, FormGroup, FormGroupDirective, FormBuilder, FormControl, Validators, EmailValidator, FormsModule, NgForm } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'dashboard-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeDetailComponent implements OnInit,OnChanges {
  
  public customLayout: boolean;
  public step = 0;
  public id: number;
  public age: number;
  public domain: string;
  public employee: Employee;
  @Input() employeeID: number;
  public employeeForm: FormGroup;
  public loading:boolean;

  constructor(
    private layoutService: LayoutService,
    private route: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit() {
    // localStorage.setItem('domain','democompanyinc');
    this.domain = localStorage.getItem('domain');
    
    this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
    });

      // this.id = this.employeeID;
      // this.getEmployee(this.employeeID);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes,'changes');
    if(!changes.firstChange){
      console.log(changes.employeeID.currentValue,'changes.currentValue');
      this.getEmployee(changes.employeeID.currentValue);
    }
  }

  setStep(index: number) {
        this.step = index;
  }

  getEmployee(id:any){
    this.loading = true;

    this.userService.getUser(id).subscribe( 
        (res) => {
          
          this.employeeForm = new FormGroup({
                    'first_name': new FormControl(res.user_data.first_name, [Validators.required,]),
                    'last_name': new FormControl(res.user_data.last_name, [Validators.required,]),
                    'middle_name': new FormControl(res.user_data.middle_name, []),
                    'birth_date': new FormControl(res.user_data.birth_date, [Validators.required,]),
                    'contact_no': new FormControl(res.user_data.contact_no, [Validators.required,]),
                    'city': new FormControl(res.user_data.birth_date, [Validators.required,]),
                    'address': new FormControl(res.user_data.birth_date, [Validators.required,]),
                    'zip': new FormControl(res.user_data.birth_date, [Validators.required,]),
                });

          this.employee = res;
          this.loading = false;
        },
        (err) => {
          console.log("Error:"+err.status);
        }
      );
  }

  setAge()
  {
    this.age = moment().diff(this.employee.birth_date, 'years', true) | 0;
  }

}


