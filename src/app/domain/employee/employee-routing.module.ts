import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeComponent } from './employee.component';
import { AuthGuard } from '../../auth';
import { AuthDomainGuard } from '../../component/auth/auth-domain';
import { EmployeeDetailComponent } from './employee-detail.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    // data: {
    //     title: 'Employee',
    //     breadcrumbs: 'employee',
    //     customLayout: true
    // },
    component: EmployeeComponent,
    // children:[
    //    {
    //     path: ':id',
    //     loadChildren: './employee-detail.module#EmployeeDetailModule',
    //     data: {
    //       title: 'Employee Data',
    //       breadcrumbs: ':id'
    //     },
    //   },
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
