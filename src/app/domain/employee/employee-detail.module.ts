import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule, FormGroup, FormBuilder, FormControl, Validators, EmailValidator,
    FormGroupDirective, NgForm, } from '@angular/forms';

import { adminLteConf } from '../../admin-lte.conf';

import { EmployeeDetailRoutingModule } from './employee-detail-routing.module';
import { CoreModule } from '../../core/core.module';

import { LayoutModule } from 'angular-admin-lte';

import { EmployeeDetailComponent } from './employee-detail.component';
import { HomeComponent } from '../../home/home.component';

import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';

// Guard
import { AuthGuard } from '../../auth';
import { AuthDomainGuard } from '../../component/auth/auth-domain';

// Services
import { GlobalRoutesService } from '../../services/route.global';
import { ClientGlobalRoutesService } from '../../services/client.global';
import { AuthService } from '../../services/auth/auth.service';
import { UserService } from '../../services/user/user.service';

// Material
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmployeeDetailRoutingModule,
    CoreModule,
    LoadingPageModule, MaterialBarModule,
    MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  ],
  declarations: [
  ],
  providers:[
    AuthGuard,
    AuthDomainGuard,
    ClientGlobalRoutesService,
    GlobalRoutesService,
    AuthService,
    UserService
  ],
})
export class EmployeeDetailModule {}
