import { Component, OnInit, AfterViewInit, ViewChild, Input, Output, ElementRef, EventEmitter, HostListener } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { PageEvent } from '@angular/material';
// import { DepartmentService   } from '../services/department/department.service';
import { UserService  } from '../../services/user/user.service';
import { LayoutService } from 'angular-admin-lte';
import { ReactiveFormsModule, FormGroup, FormGroupDirective, FormBuilder, FormControl, Validators, EmailValidator, FormsModule, NgForm } from '@angular/forms';
import { PaginationInstance } from 'ngx-pagination';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Employee } from '../../models/user/employee/index';
import { Observable } from 'rxjs';
import { EmployeeDetailComponent } from './employee-detail.component';
@Component({
  selector: 'dashboard-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'firstname', 'lastname', 'email', 'department', 'job_title', 'status'];
  dataSource: MatTableDataSource<Employee>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public customLayout: boolean;
  public employees: Employee[];
  public domain = localStorage.getItem('domain');
  // MatPaginator Inputs
  length = 5;
  pageSize = 5;
  pageSizeOptions: number[] = [1, 5, 10, 15, 25, 100];

    @Input() id: string;
    // @Input() maxSize: number;
    @Output() pageChange: EventEmitter<number>;
    page: number = 1;
    model: any = {};
    returnUrl: string;
    loading = true;
    filter: string;
    // @Input('data') employees: Employee[] = [];
    totalList: number;
    currentPage: number;
    per_page: number;
    onSearch: boolean;
    shortResultSearch: boolean;
    viewingEmployee:  boolean;
    @Input('employeeID') employeeID: number;
    
    // public filter: string = '';
    public maxSize: number;
    public directionLinks: boolean = true;
    public autoHide: boolean = false;
    public config: PaginationInstance = {
        id: 'advanced',
        itemsPerPage: 5,
        currentPage: 1,
        totalItems: null
    };
  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(
    private layoutService: LayoutService,
    private router: Router,
    private userService:  UserService,
  ) {

    this.viewingEmployee = false;

  }

  ngOnInit() {
    
    this.filter = "";

    this.currentPage = 1;
    this.per_page = 5;
    this.onSearch = false;

    // this.domain = localStorage.getItem('domain');

    this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
    });

    this.getEmployees();

  }

  getEmployees(){
    this.userService.getEmployees(this.currentPage, this.per_page, this.filter).subscribe( 
        (res)  =>  {
          
          this.loading = true;
          // this.pageSize = res.per_page;
          this.length = res.total;
          this.employees = res.data;
          this.dataSource = new MatTableDataSource(res.data);

          this.loading =  false;

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;

        },
        (err)  =>  {
          console.log(err);
        }
    );
  }

  applyFilter(filterValue: string) {
    this.loading = true;
    this.viewingEmployee = filterValue != null ? false : true;
    
    var filterVal = filterValue.toLowerCase();
    this.userService.getEmployees(this.page, this.per_page, filterVal).subscribe( 
        (res)  =>  {
          console.log(res,'new employee list');
          this.length = res.total;
          this.employees = res.data;
          this.dataSource = new MatTableDataSource(res.data);

          this.loading =  false;
        },
        (err)  =>  {
          console.log("Error:"+err.status);
        }
        );

  }

  //Pagination Section
    onPageChange(event) {
       // alert(JSON.stringify("Current page index: " + event.pageIndex));
       this.currentPage = event.pageIndex+1;
        console.log(event,'event');
        this.per_page = event.pageSize;
        // this.config.currentPage = number;
        // this.getDoctors(number);
      this.filter = this.filter !== null ? this.filter : "";
      
      this.userService.getEmployees(event.pageIndex+1, event.pageSize, this.filter).subscribe( 
        (res)  =>  {
          console.log(res,'new employee list');
          this.length = res.total;
          this.employees = res.data;
          this.dataSource = new MatTableDataSource(res.data);

          this.loading =  false;
        },
        (err)  =>  {
          console.log("Error:"+err.status);
        }
        );
    }

  
  ngAfterViewInit() {

        // if(navigator.getUserMedia && navigator.mediaDevices.getUserMedia) {
        //     navigator.mediaDevices.getUserMedia({ video: false, audio: true }).then(stream => {
        //         // console.log(this.video);
        //         this.video.nativeElement.srcObject = stream;
        //         this.video.nativeElement.play();

        //     });
        // }

    }

    setPageSizeOptions(setPageSizeOptionsInput: string) {
      console.log(setPageSizeOptionsInput);
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }



  viewEmployeeDetail(employee:any){
    
    this.viewingEmployee = true;
    this.employeeID = employee.id;
    // this.employeeDetailComponent.viewEmployee(employeeID);
    
    // this.userService.setViewedEmployee(employeeID);

    console.log(this.employeeID, 'employeeID');
  }

  viewEmployeeFullDetail(id:number)  {
    event.stopPropagation();
    if(id == JSON.parse(localStorage.getItem("user")).id){
      // this.router.navigate([this.domain + '/dashboard/profile']);
      this.router.navigate([this.domain + '/dashboard/employee', id]);
    } else {
      this.router.navigate([this.domain + '/dashboard/employee', id]);
    }

    return false;
  }

}
