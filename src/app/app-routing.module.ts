import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth';
import { AuthDomainGuard } from './component/auth/auth-domain';
import { UnAuthDomainGuard } from './component/auth/unauth-domain';
import { LayoutModule } from 'angular-admin-lte';
import { adminLteConf } from './admin-lte.conf';

import { LoginComponent } from './+login/login.component';
import { LoginUnAuthDomainComponent } from './auth/login-unauth-domain.component';

const routes: Routes = [
  // {
  //   path: 'login',
  //   loadChildren: './+login/login.module#LoginModule',
  //   data: {
  //     customLayout: true
  //   }
  // }, 
  // {
  //   path: 'register',
  //   loadChildren: './+register/register.module#RegisterModule',
  //   data: {
  //     customLayout: true
  //   }
  // },
  {
    path: '',
    data: {
        title: 'Get Started',
        breadcrumbs: '',
        customLayout: true

    },
    component: HomeComponent,
  },

  {  
        path: 'login',
        canActivate:[UnAuthDomainGuard],
        component:LoginUnAuthDomainComponent,
        data: {
          customLayout: true
        }
        // loadChildren: './auth/login-unauth-domain.module#LoginUnAuthDomainModule',
        // data: {
        //   customLayout: true
        // }
  },
  {  

        path: ':domain/login',
        canActivate:[AuthDomainGuard],
        component: LoginComponent,
        // loadChildren: './+login/login.module#LoginModule',
        data: {
          customLayout: true
        }
      }, 
      {
        path: ':domain/register',
        canActivate:[UnAuthDomainGuard],
        loadChildren: './+register/register.module#RegisterModule',
        // data: {
        //   title: ':domain registration',
        //   breadcrumbs: ':domain registration',
        //   customLayout: true
        // }
  },
  {
    path: ':domain',
    // data: {
    //     title: ':domain',
    //     breadcrumbs: ':domain',
    // },
    canActivate: [AuthGuard],
    children: [

      {
        path: 'dashboard',
        loadChildren: './domain/dashboard/dashboard.module#DashboardModule',
        // data: {
        //   title: 'Dashboard',
        //   breadcrumbs: 'dashboard',
        // },
      },

    ]
  },
  {
    path: ':domain/dashboard',
    // data: {
    //     title: 'dashboard',
    //     breadcrumbs: 'dashboard',
    // },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'employee',
        loadChildren: './domain/employee/employee.module#EmployeeModule',
        // data: {
        //   title: 'Employee List',
        //   breadcrumbs: 'employee'
        // },
      },
      {
        path: 'employee/:id',
        loadChildren: './domain/employee/employee-detail.module#EmployeeDetailModule',
        // data: {
        //   title: 'Employee Information',
        //   breadcrumbs: 'employee/:id/info'
        // },
      },
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
