import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginUnAuthDomainComponent } from './login-unauth-domain.component';

const routes: Routes = [{
  path: '',
  component: LoginUnAuthDomainComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginUnauthDomainRoutingModule { }
