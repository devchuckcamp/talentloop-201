import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginUnauthDomainRoutingModule } from './login-unauth-domain-routing.module';
import { LoginUnAuthDomainComponent } from './login-unauth-domain.component';
import { FormsModule,ReactiveFormsModule }    from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    LoginUnauthDomainRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LoginUnAuthDomainComponent]
})
export class LoginUnAuthDomainModule { }
