import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';


import { GlobalRoutesService } from '../services/route.global';
import { ClientGlobalRoutesService } from '../services/client.global';
import { AuthService } from '../services/auth/auth.service';

import { LoggedIn } from '../models/loggedin/index';

import { FormGroup, FormBuilder, FormControl, Validators, EmailValidator } from '@angular/forms';

@Component({
  selector: 'app-login-unauth-domain',
  templateUrl: './login-unauth-domain.component.html',
  styleUrls: ['./login-unauth-domain.component.css']
})
export class LoginUnAuthDomainComponent implements OnInit {
	domain:	string;
	model: any = {};
    returnUrl: string;
    loading = false;
    meImg = '';
    Bearer:any;
    invalidCredentials = true;
    error : any =  Object;
    isLinear = true;
    withDomain:boolean;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    domainLoginForm:FormGroup;
    totalStepsCount: number;

  	constructor(
  		private clientService:ClientGlobalRoutesService,
  		private authService: AuthService,
  		private formBuilder: FormBuilder,
  		public router: Router,
        private route: ActivatedRoute,
        private http: Http,
  	) { }

  ngOnInit() {
  	this.domain = "";
  	this.domain = localStorage.getItem('domain');
  	let clientKey	=	this.clientService.getClientKey();
  	let domain	=	this.authService.checkDomain('democompany').subscribe( (res)=>{
  		
  	});
  	// let auth	=	this.authService.loginAuth('admin@gmail.com','dChuckie123!').subscribe( (res) => {

  	// });

  	this.domainLoginForm = this.formBuilder.group({
          domain: ['', [Validators.required]],
        });
  }

   	loginDomain(){
      

            
        if(	this.domainLoginForm.controls.domain.valid ){
              this.loading	= true;
             
              // this.authService.loginAuth(this.domainLoginForm.value.domain,this.domainLoginForm.value.password).subscribe(
              //     (res) => {
              //       // console.log("Loggin In");
              //       // console.log(res.status,"login data");
              //       if(res.access_token){
                      
                      
              //         localStorage.setItem('isLoggedin','true');
              //         localStorage.setItem('currentUser',res.access_token);

              //         this.authService.getUser(res.access_token).subscribe(
              //             response =>
              //             { 

              //               var user = {
              //                 "id": response.id,
              //                 "username": response.username,
              //                 "name": response.name,
              //                 "email": response.email,
              //                 "role_id": response.role_id,
              //                 "status": response.status,
              //                 "remember_token": response.refresh_token,
              //                 "user_id": res.id,
              //                 "client_id": response.client_id,
              //               }
              //               console.log(response,'response');
              //               localStorage.setItem('user',JSON.stringify(response));
              //               //this.router.navigate(['/dashboard']);
              //               if(this.route.snapshot.queryParams['returnUrl']){
              //                 this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
              //                 if(this.returnUrl=='/'){
              //                   this.router.navigate([this.domain+'/dashboard']);
              //                 }
              //                 this.router.navigate([this.domain+this.returnUrl]);
                              
              //               }else{

              //                 this.router.navigate([this.domain+'/dashboard']);
                              
              //               }
              //             }
              //           );
                      
                      
              //       } else if( res.status == 401) {
                    	
              //         this.invalidCredentials = true;
              //         this.loading = false ;
              //       }
                    
              //     },
              //     error =>
              //     {
                	
              //       if(error.status == "401"){
              //       	console.log(error);
              //       	console.log(error._body.error);
              //         	this.invalidCredentials = true; 
                      
              //         // this.snackBar.open('Invalid Credentials', 'X', {
              //         //   duration: 3000,
              //         //   direction: "ltr",
              //         //   verticalPosition:"top",
              //         //   horizontalPosition: "right"
              //         // });

              //         this.loading = false;
              //         this.error.hasError = true;
              //         this.error.message = "Username or password is not valid";
              //       }

              //       this.loading = false ;
              //     },
                  
              //   );

            this.authService.checkDomain(this.domainLoginForm.value.domain).subscribe( res =>{
              console.log(res,'res');
              
              

              if(res.status == 200){
                localStorage.setItem('domain',res.domain_name);
                
                if(this.route.snapshot.queryParams['returnUrl']){
                  
                  // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
                  this.router.navigate([res.domain_name+'/login']);
                  
                    
                  
                              
                }else{

                  this.router.navigate([res.domain_name+'/login']);
                              
                }

              }  else {
                console.error('Domain not found');
              }

            },
            error  =>  {
              console.log(error,'error');
            })
        }else{
           	this.model.username  = '';
            this.model.password  = ''
            this.loading = false ;
        }

      	return false;
    }

}