import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'angular-admin-lte';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public customLayout: boolean;
  public authDomain: boolean;
  public authUser: boolean;

  constructor(
    private layoutService: LayoutService,
    private _router: Router
  ) {
    
  }

  ngOnInit() {
    this.authUser =false;
    this.authDomain = false;

    this.layoutService.isCustomLayout.subscribe((value: boolean) => {
      this.customLayout = value;
      // console.log(value);
    });

    this._router.events.subscribe((val) => {
        let fullRoute = this._router.url;
        let currentURL = fullRoute.substring(0, fullRoute.indexOf('?')) ? fullRoute.substring(0, fullRoute.indexOf('?')) : fullRoute;
        this.authDomain =false;
        this.authUser =false;

        if( localStorage.getItem('domain') ){
            this.authDomain = true;
        }else{
            this.authDomain =  false;  
        }


        if( localStorage.getItem('currentUser') ){
            this.authUser = true;
        }else{
            this.authUser =false;  
        }
    });
    

  }
}
