import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { adminLteConf } from './admin-lte.conf';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';

import { LayoutModule } from 'angular-admin-lte';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { LoadingPageModule, MaterialBarModule } from 'angular-loading-page';


//Authorization
import { LoginModule } from './+login/login.module';
import { LoginComponent } from './+login/login.component';

// Module
//=============  Dashboard ==============//
import { LoginUnAuthDomainModule } from './auth/login-unauth-domain.module';
//======================================//

//=============  Dashboard ==============//
import { DomainModule } from './domain/domain.module';
import { DashboardModule } from './domain/dashboard/dashboard.module';
import { DashboardComponent } from './domain/dashboard/dashboard.component';
//====================================//

//============= Employee  ============//
import { EmployeeModule } from './domain/employee/employee.module';
import { EmployeeRoutingModule } from './domain/employee/employee-routing.module';
import { EmployeeComponent } from './domain/employee/employee.component';
import { EmployeeDetailModule } from './domain/employee/employee-detail.module';
import { EmployeeDetailRoutingModule } from './domain/employee/employee-detail-routing.module';
import { EmployeeDetailComponent } from './domain/employee/employee-detail.component';
//===============================//

// Guard
import { AuthGuard } from './auth';
import { AuthDomainGuard } from './component/auth/auth-domain';
import { UnAuthDomainGuard } from './component/auth/unauth-domain';

// Services
import { LayoutService } from 'angular-admin-lte';

import { GlobalRoutesService } from './services/route.global';
import { ClientGlobalRoutesService } from './services/client.global';
import { AuthService } from './services/auth/auth.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


// Material
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    AppRoutingModule,
    DomainModule,
    DashboardModule,
    EmployeeModule,
    EmployeeDetailModule,
    CoreModule,
    LayoutModule.forRoot(adminLteConf),
    LoadingPageModule, MaterialBarModule,
    LoginUnAuthDomainModule,
    LoginModule
  ],
  exports:[
    LayoutModule,
    CoreModule,
    LoadingPageModule, 
    MaterialBarModule,
    AppComponent,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  providers:[
    AuthGuard,
    AuthDomainGuard,
    UnAuthDomainGuard,
    ClientGlobalRoutesService,
    GlobalRoutesService,
    AuthService,
    LayoutService,
    // UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
