import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class DomainGuard implements CanActivate {

    constructor(private router: Router) { 
    	
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log(state.url);
        if (localStorage.getItem('domain')) {
            return true;
        }
        
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return true;
    }
}
