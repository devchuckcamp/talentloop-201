import { ErrorHandler, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable, Subject, throwError } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { map, take, catchError } from 'rxjs/operators';

//Model
import { Credential } from '../../models/user/';
import { ClientGlobalRoutesService } from '../client.global';
import { GlobalRoutesService } from '../route.global';

@Injectable()
export class UserService {

    private clientID = 'newclient';
    private Bearer: any;
    credentials: Credential;
    private userID = '';
    //public apiPort='8181';
    public apiPort = ''; //for staging 
    //public host='43.226.6.141';
    public host='api.talentloop.com'; //for staging
    public protocol='http://';

    constructor(
        private http: Http,
        private clientGlobalRoutesService:ClientGlobalRoutesService,
        private globalRoutesService: GlobalRoutesService

    ) {
        if (localStorage.getItem("currentUser")) {
            this.Bearer = localStorage.getItem("currentUser");
        }

    }



    validate_activation_token(token:string){

        return this.http.get(this.protocol+this.host+':'+this.apiPort+'/api/v1/account/activation'+'?token='+token            , this.noAuthHeadjt())
            .map((response: Response) => response.json());
    }

    canEditData(id:number){
        let form  = JSON.stringify({
            profile_id:id
        });
        return this.http.post(this.protocol+this.host+':'+this.apiPort+'/api/v1/profile/editor',
            form
            , this.jt())
            .map((response: Response) => response.json());
    }

    postProfileFile(fileToUpload: File, profile_id: number){
        const uri =  this.protocol+this.host+this.apiPort+'/api/v1/profile/image/upload';
        const formData: FormData = new FormData();

        formData.append('image', fileToUpload, fileToUpload.name);
        // formData.append('profile_id' );
        let body = {
            photo:fileToUpload.name,
            profile_id:profile_id
        }
        return this.http
          .post(uri+'?id='+profile_id, formData, this.fileHeader())
          .map((response: Response) => { return response.json() });
    }


    getFile(file: string){
        const uri =  this.protocol+this.host+this.apiPort+'/api/v1/profile/image';
        return this.http
          .get(uri, this.fileHeader())
          .map((response: Response) => { return response.json() });
    }

    validatePassword(password:string){
        let form  = JSON.stringify({
            password:password
        });
        return this.http.post(this.protocol+this.host+':'+this.apiPort+'/api/v1/validate/password',
            form
            , this.jt())
            .map((response: Response) => response.json());
    }

    getUser(id: number) {
        this.Bearer = localStorage.getItem("currentUser");
        return this.http.get(this.protocol+this.host+':'+this.apiPort+'/api/v1/users/' + id 
            , this.jt())
            .map((response: Response) => response.json());
    }

    

    getUserRole() {
        return JSON.parse(localStorage.getItem("user"));
    }

    createUser(form: object) {
        return this.http.post(this.protocol+this.host+':'+this.apiPort+'/api/v1/users',
            form
            , this.jt())
            .map((response: Response) => response.json());
    }

    // no Get Users

    getCredential(id: number) {
        return this.http.get(this.protocol+this.host+':'+this.apiPort+'/api/v1/auth-user', this.jt())
            .map((response: Response) => response.json())
            .do(data => JSON.stringify(data)); //console.log('All: ' + JSON.stringify(data)))

    }
    updateCredential(id:number, new_password: string, old_password: string){
        let body = {
            id: id,
            password: new_password,
            old_password: old_password,
        }

        return this.http.put(this.protocol+this.host+':'+this.apiPort+'/api/v1/users/'+id, body, this.jt())
        .map((response: Response) => response.json())
        .do(data => JSON.stringify(data)); //console.log('All: ' + JSON.stringify(data)))
    }

    activateAccountPassword(id:number, password: string){
        let body = {
            id:id,
            password: password,
            status:1
        }

        return this.http.put(this.protocol+this.host+':'+this.apiPort+'/api/v1/account/activate/'+id, body, this.noAuthHeadjt())
        .map((response: Response) => response.json())
        .do(data => JSON.stringify(data)); //console.log('All: ' + JSON.stringify(data)))
    }

    //Patient Credential
    getPatientCredential(id: number) {
        return this.http.get(this.protocol+this.host+':'+this.apiPort+'/api/v1/patients/' + id, this.jt())
            .map((response: Response) => response)
            .do(data => JSON.stringify(data)); //console.log('All: ' + JSON.stringify(data)))

    }
    //Doctor Credential
    getDoctorCredential(id: number) {
        return this.http.get(this.protocol+this.host+':'+this.apiPort+'/api/v1/doctors/' + id, this.jt())
            .map((response: Response) => response)
            .do(data => JSON.stringify(data)); //console.log('All: ' + JSON.stringify(data)))

    }

    createCredential(body: Object) {

    }

    getEmployees(page: number, perPage: number, filter: string){
        var filter = filter !== "" ? '&filter='+filter : "";
        var uri  = this.globalRoutesService.getAllEmployeesURI();
        return this.http.get(
                uri+'?page='+page+'&per_page='+perPage+filter,
                this.jt())
            .map((response: Response) =>response.json());
    }

    getEmployee(id: number){
        var uri  = this.globalRoutesService.getAllUsersURI();
        return this.http.get(
                uri+'/'+id+'?type=employee',
                this.jt())
            .map((response: Response) =>response.json());
    }



    //For Segragation -> Routes
    private jt() {

        let UserBearer = this.Bearer;
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.Bearer });
        headers.append('Content-type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Allow_Headers', ' Allow, Access-Control-Allow-Origin, Content-type, Accept');
        headers.append('Allow', 'GET,POST,PUT,DELETE,OPTION');
        headers.append('Access-Control-Allow-Origin', '*');

        return new RequestOptions({ headers: headers });
    }

    private plain() {
        let header = new Headers({ 'Accept': 'application/json' });
        header.append('Content-type', 'application/json');


        return new RequestOptions({ headers: header });
    }
    //To be move to a single module
    private fileHeader() {

        let UserBearer = this.Bearer;
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.Bearer });
        headers.append('Content-type', 'multipart/form-data;x-www-form-urlencoded');
        headers.append('Accept', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Allow_Headers', ' Allow, Access-Control-Allow-Origin, Content-type, Accept');
        headers.append('Allow', 'GET,POST,PUT,DELETE,OPTION');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.delete('Content-Type');

        return new RequestOptions({ headers: headers });
    }

    private noAuthHeadjt() {
        let headers = new Headers();
        headers.append('Content-type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Allow_Headers', ' Allow, Access-Control-Allow-Origin, Content-type, Accept');
        headers.append('Allow', 'GET,POST,PUT,DELETE,OPTION');

        return new RequestOptions({ headers: headers });

    }

}