import { ErrorHandler, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable, Subject, throwError } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { map, take, catchError } from 'rxjs/operators';

// import * as xml2js from 'xml2js';
import { User } from '../../models/user/';

//GLobal
import { GlobalRoutesService } from '../route.global';


interface MyModel {
  foo: string;
  oo: string;
}

@Injectable()
export class DomainAuthService {

    private clientID = 'newclient';
    //private storage = localStorage.getItem('currentUser').json();
    private Bearer:any;
    
    private userID = '';

    constructor
    (
        private http: Http,
        private httpClient: HttpClient,
        private globalRoutesService:GlobalRoutesService
    ) {
        if(localStorage.getItem("currentUser")){
            this.Bearer = localStorage.getItem("currentUser");
        }
        //console.log(this.Bearer); 
        //console.log(this.globalRoutesService.getLoginURI());
    }

    // Error Handling
    // handleError(errorResponse: HttpErrorResponse){
    //     if(errorResponse.error instanceof ErrorEvent){
    //         console.error("Client Side Error:", errorResponse.error.message);
    //     } else {
    //         console.error("Server Side Error:", errorResponse);
    //     }

    //     return errorResponse(errorResponse);
    // }

    // Check Domain
    checkDomain(domain:string){
        let uri = this.globalRoutesService.getDomainURI();
        return this.http.get(
            uri+'?domain='+domain,
            this.jt())
            .map((response: Response) => 
                    response.json()
                );
    }

    //Get User
    getUser(token:string){
        let uri = this.globalRoutesService.getUsersURI();
        return this.http.get(
            uri,
            this.custom_jt(token))
            .map((response: Response) => 
                    response.json()
                    
                );
    }
    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
        // return error;
      }
      // return an observable with a user-facing error message
      return throwError(
        error);
    };
    loginAuth(username:string,password:string): Observable<any> {
        let uri = this.globalRoutesService.getLoginURI();
        let client = JSON.parse(this.globalRoutesService.getClientKey() );

        var body = JSON.stringify({
                username:username,
                password:password,
                client_id:client.client_id,
                client_secret:client.client_secret,
                grant_type:client.grant_type
            });

        

        return this.http.post(uri,
            body
            ,this.jt())
            .pipe(
                catchError(this.handleError),
                map((response: Response) => 
                    response.json())
            );
    }

    //Google Login
    getGoogleLogin(){
        var googleURI = this.globalRoutesService.getGoogleLogin();
        //console.log(googleURI);

        return this.http.get(
            googleURI
            ,this.plain())
            .map((response: any) => 
                    {
                        //console.log(response);
                        window.open(response);
                    }
                );
    }
                        
    //Custom Token
    private custom_jt(token:string) {
       
        let UserBearer = this.Bearer;
            let headers = new Headers({'Authorization': 'Bearer '+token });
            headers.append('Content-type','application/json');
            headers.append('Accept','application/json');
            headers.append('Access-Control-Allow-Origin','*');
            headers.append('Allow_Headers',' Allow, Access-Control-Allow-Origin, Content-type, Accept');
            headers.append('Allow','GET,POST,PUT,DELETE,OPTION');
            headers.append('Access-Control-Allow-Origin','*');
             
            return new RequestOptions({ headers: headers });
    }
    private jt() {
       
        let UserBearer = this.Bearer;
            let headers = new Headers({'Authorization': 'Bearer '+this.Bearer });
            headers.append('Content-type','application/json');
            headers.append('Accept','application/json');
            headers.append('Access-Control-Allow-Origin','*');
            headers.append('Allow_Headers',' Allow, Access-Control-Allow-Origin, Content-type, Accept');
            headers.append('Allow','GET,POST,PUT,DELETE,OPTION');
            headers.append('Access-Control-Allow-Origin','*');
             
            return new RequestOptions({ headers: headers });
    }

    private plain() {
            let header = new Headers({'Accept':'application/json'});
            header.append('Content-type','application/json');
            
            return new RequestOptions({ headers: header });
    }
}