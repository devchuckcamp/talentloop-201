import { Component, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import * as Prism from 'prismjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {

  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {
    Prism.highlightAll();
  }

  constructor(
  	private router: Router,
  	){

  	let domain = localStorage.getItem('domain');
  	
  	if (domain !== null) {
  		
        if (localStorage.getItem('currentUser') !== null) {

        	this.router.navigate([domain+'/login']);
        }
        this.router.navigate(['login']);
        // return false;   

        
    }
        
  }
}
