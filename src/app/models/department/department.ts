export class Department {
    id: number;
    name:string;
    description:string;
    color:string;
    client_id:number;
    users:Array<any>;
}