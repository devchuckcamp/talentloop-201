export class Doctor {
    id: number;
    fname:string;
    lname:string;
    email:string;
    contact_no:string;
    license_no:string;
    gender:string;
    birth_date:string;
    specialty_id:number;
    clinic_id: number;
    zip:string;
    city:string;
    address:string;
    image_url:string;
    doctorspecialty:Object;
    clinic:Object;
    user_id:number;
}