export class Clinic {
    id: number;  
    name: string;
    desciption:string;
    address:string;
    city:string;
    zip: string;
    contact_no: string;
    start_time:string;
    end_time:string;
    longitude:string;
    latitude: string;
}