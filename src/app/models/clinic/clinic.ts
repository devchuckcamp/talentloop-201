export class Clinic {
    id: number;
    name:string;
    description:string;
    start_time:string;
    end_time:string;
    contact_no:string;
    longitute:number;
    latitude:number;
    address:string;
    city:string;
    zip:string;
    image_url:string;
    doctor:Array<any>;


}