export class SocialHealth{
    patient_id: number;
    alcohol_status: string;
    smoking_status: string;
}