export class Medication{
    patient_id:number;
    medication_taking:string;
    start_date:string;
    end_date:string;
    dosage:string;
}