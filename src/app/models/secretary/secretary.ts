
export class Secretary {
    id: number;
    fname:string;
    lname:string;
    mname:string;
    suffix:string;
    birth_date:string;
    contact_no:string;
    email:string;
    gender:string;
    address:string;
    city:string;
    zip:string;
    patientcurrentmedication:string;
    patientprescription:Array<any>;
    prescriptionitems:Array<any>;    
}