export class Credential {
    id: number;
    username: string;
    name: string;
    email: string;
    password: string;
    confirm_password:string;
    role_id: number;
    status: number;
}