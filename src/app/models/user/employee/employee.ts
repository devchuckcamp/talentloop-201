export class Employee {
    id: number;
    email:string;
    contact_no:string;
    gender:string;
    birth_date:string;  
    zip:string;
    city:string;
    address:string;
    image_url:string;
    user_data:any;
    user_id:number;
}