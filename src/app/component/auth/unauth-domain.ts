import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Injectable()
export class UnAuthDomainGuard implements CanActivate {
    private domain:    string;

    constructor(
        private router: Router,
        private http: Http,
        private route: ActivatedRoute,
        ) { 
        
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // console.log(state.url);
        this.route.params.subscribe(params => {

            if (localStorage.getItem('domain')!== null) {
                
                // this.navigated = false;
                this.domain = localStorage.getItem('domain');
                this.router.navigate([localStorage.getItem('domain')+'/login']);
                return false;

            } else {
  
                return true;
            
            }

        });
        // alert(localStorage.getItem('domain'));
        // return false;
        // this.router.navigate([this.domain+'/login']);
        return true;
    }
}
