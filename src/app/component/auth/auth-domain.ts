import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Injectable()
export class AuthDomainGuard implements CanActivate {
    // private domain:    string;
    public domain = localStorage.getItem('domain');

    constructor(
        private router: Router,
        private http: Http,
        private route: ActivatedRoute,
        ) { 
        
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log(state.url);
        // this.route.params.subscribe(params => {
            if (this.domain !== null) {
                
                if (localStorage.getItem('currentUser') !== null) {
                    this.router.navigate([this.domain+'/dashboard']);
                }
                // this.domain = params['domain'];
               // this.domain = localStorage.getItem('domain');
               console.log(localStorage.getItem('domain'));
               return true;
            }
            
            

        //});
        // return true;
        // this.router.navigate([this.domain+'login']);
        return false;
    }
}