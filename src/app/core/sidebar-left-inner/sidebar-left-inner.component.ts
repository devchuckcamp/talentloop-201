import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
@Component({
  selector: 'app-sidebar-left-inner',
  templateUrl: './sidebar-left-inner.component.html'
})
export class SidebarLeftInnerComponent implements OnInit {
	
	domain:string;

	constructor(
		private router: Router,
		){

	}

	ngOnInit(){
		this.domain = localStorage.getItem('domain');
	}
}
