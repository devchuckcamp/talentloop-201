#Talentloop 201

## Doc & Demo
## Themes
[https://mika-el.github.io/angular-admin-lte](https://mika-el.github.io/angular-admin-lte/)

## Talenloop 201
```
	- Accessible simultaneously by employee and admin with their credentials
	- HRIS

```

## Modules
```
	-	Dashboard
	-	Employee
	-	Timekeeping
	-	
```

# Process Flow -

# Roles:

##	Superadmin
```
 	Can "CRUD" an Admin/Supervisor/Manager/Employee Account
	Manage Company Profile Settings
	Can restrict user access
	View/Manage User's Page
	Can generate reports
	Can Post/Edit company post/announcement
	Can view logs from all clients
	Overried Admin Access	
```

##	Admin
```	Can "CRUD" an Admin/Supervisor/Manager/Employee Account
	Can assign Employee direct reports (Manager/Supervisor)
 	Manage Company Profile Settings
 	Can restrict user access
 	View/Manage User's Page
	Can generate reports
	Can Post/Edit company post/announcement within the company
 	Can view timestamps IN/OUT within the company
 	Can view logs within the company
```

##	Manager
```	Can "CRU" a Supervisor/Employee Account
 	Can generate report for employee reporting under him/her
 	Can view timestamps IN/OUT (with PROS & CONS) of Supervisors/Employees under him/her.
```

##	Supervisor
```	Can "CRU" a Supervisor/Employee Account
	Can generate report for employee reporting under him/her
	Can view timestamps IN/OUT (with PROS & CONS) of Supervisors/Employees under him/her.
```	


## 	Employee
```	Can "View" Profile
	Can apply request (leaves, schedule adjustment)
```

##Architecture
```	Cloud-based:	```
```	Azured	```